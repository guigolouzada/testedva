<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM contato WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$pessoa = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['nome']) && isset($_POST['username'])&& isset($_POST['email'])&& isset($_POST['telefone']) ) {
  $nome = $_POST['nome'];
  $username = $_POST['username'];
  $email = $_POST['email'];
  $telefone = $_POST['telefone'];
  
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nome' => $nome, ':username' => $username, ':email' => $email,':telefone' => $telefone, ':id' => $id])) {
    header("Location: /");
  }



}


 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Contato Selecionado</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="nome">Nome</label>
          <input value="<?= $pessoa->nome; ?>" type="text" nome="nome" id="nome" class="form-control">
        </div>
        <div class="form-group">
          <label for="username">Nome de Usuário</label>
          <input value="<?= $pessoa->username; ?>" type="text" name="username" id="username" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" value="<?= $pessoa->email; ?>" name="email" id="email" class="form-control">
        </div>
        <div class="form-group">
          <label for="telefone">Telefone</label>
          <input type="telefone" value="<?= $pessoa->telefone; ?>" name="telefone" id="telefone" class="form-control">
        </div>
        <div class="form-group">
          <a class="nav-link" href="index.php">Voltar</a>
        </div>
      </form>
    </div>
  </div>
</div>
<?php require 'footer.php'; ?>
