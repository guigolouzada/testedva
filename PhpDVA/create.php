<?php
require 'db.php';
$message = '';
if (isset ($_POST['nome']) && isset($_POST['username']) && isset($_POST['email'])&& isset($_POST['telefone']) ) {
  $nome = $_POST['nome'];
  $username = $_POST['username'];
  $email = $_POST['email'];
  $telefone = $_POST['telefone'];
  $sql = 'INSERT INTO contato(nome,username,email, telefone) VALUES(:name,:username,:email,:telefone)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':name' => $name, ':email' => $email])) {
    $message = 'data inserted successfully';
  }



}


 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Criar Contato</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post">
        <div class="form-group">
          <label for="nome">Nome</label>
          <input type="text" nome="nome" id="nome" class="form-control">
        </div>
        <div class="form-group">
          <label for="username">Nome do Usuário</label>
          <input type="text" username="username" id="username" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" class="form-control">
        </div>
        <div class="form-group">
          <label for="telefone">Telefone</label>
          <input type="telefone" name="telefone" id="telefone" class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Criar Contato</button>
           <a class="nav-link" href="index.php">Voltar</a>
        </div>
      </form>
    </div>
  </div>
</div>
<?php require 'footer.php'; ?>
