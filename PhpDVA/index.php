<?php
require 'db.php';
$sql = 'SELECT * FROM contato';
$statement = $connection->prepare($sql);
$statement->execute();
$usuario = $statement->fetchAll(PDO::FETCH_OBJ);
?>
<?php require 'header.php'; ?>
<html>
    <head>
        <title>Teste</title>
 <style>
/* Sortable tables */
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}
 
</style>
<script src="sorttable.js"></script>

    </head>
    <body>

        <div class="container">
            <div class="card mt-5">
                <div class="card-header">
                    <h2>Contatos</h2>
                </div>
                <div class="card-body">
                    <table border="4" width="100%"   class="sortable">
                        <thead>
                            <tr align="center">
                                <th >ID</th>
                                <th>Nome</th>
                                <th >Nome de Usuário</th>
                                <th>Email</th>
                                <th >Telefone</th>
                                <th>Ação</th>
                            </tr>
                            <?php foreach ($usuario as $pessoa): ?>
                            </thead>
                            <tbody>
                                <tr align="center">
                                    <td ><?= $pessoa->id; ?></td>
                                    <td><?= $pessoa->nome; ?></td>
                                    <td><?= $pessoa->username; ?></td>
                                    <td><?= $pessoa->email; ?></td>
                                    <td><?= $pessoa->telefone; ?></td>
                                    <td>
                                        <a href="visualizar.php?id=<?= $pessoa->id ?>" class="btn btn-info">Visualizar</a>
                                        <a onclick="return confirm('tem certeza que deseja excluir?')" href="delete.php?id=<?= $pessoa->id ?>" class='btn btn-danger'>Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <?php require 'footer.php'; ?>
        
    </body>
</html>

